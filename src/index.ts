require('dotenv').config();
const Twitter = require('twitter');
const csv = require('csv');
const csvsync = require('csvsync');
const async = require('async');
const fs = require('fs');
const cron = require('node-cron');

const env = process.env;
const client = new Twitter({
  consumer_key: env.CONSUMER_KEY,
  consumer_secret: env.CONSUMER_SECRET,
  access_token_key: env.ACCESS_TOKEN,
  access_token_secret: env.TOKEN_SECRET
});

console.log('hound-dog is sleeping...🐕');
//平日
cron.schedule('0 0 5 * * 1-5', () => main());
cron.schedule('0 0 11 * * 1-5', () => main());
cron.schedule('0 0 15 * * 1-5', () => main());
cron.schedule('0 0 17 * * 1-5', () => main());
cron.schedule('0 0 20 * * 1-5', () => main());

//休日
cron.schedule('0 0 8 * * 0,6', () => main());
cron.schedule('0 0 12 * * 0,6', () => main());
cron.schedule('0 0 14 * * 0,6', () => main());
cron.schedule('0 0 16 * * 0,6', () => main());
cron.schedule('0 0 18 * * 0,6', () => main());

const getFileSync = (filename: string) => {
  const data = fs.readFileSync(filename);
  return csvsync.parse(data);
};

let tweetPost = async (content: string) => new Promise((resolve, reject) => {
  console.log('Operation: start to tweet post');
  client.post('statuses/update', {status: content}, (error: any) => !error ? resolve(content) : reject(new Error));
});

const main = () => {
  console.log('hound-dog wake up 🐕');
  const filename = 'test.csv';
  const data = getFileSync(filename);
  let targetRow: string[] = [];
  let lock = false;
  async.each(data, (element: string[], callback: any) => {
      const [postId, isPosted, content] = element;
      if (isPosted === '0' && !lock) {
        lock = true;
        targetRow.push(postId);
        targetRow.push('1');
        targetRow.push(content);
      }
      callback();
    },
    () => {
      tweetPost(targetRow[2])
        .catch(() => console.log('Operation: failed tweet'))
        .then(content => {
          console.log("Operation: tweet success");
          console.log("Operation: tweet content => " + content);
          let newData: string[][] = [];
          async.each(data, (element: string[], callback: any) => {
            let row2: string[] = [];
            if (targetRow[0] === element[0]) {
              row2.push(targetRow[0]);
              row2.push(targetRow[1]);
              row2.push(targetRow[2]);
            } else {
              row2.push(element[0]);
              row2.push(element[1]);
              row2.push(element[2]);
            }
            newData.push(row2);
            callback();
          }, () => {
            csv.stringify(newData, (error: string, output: string) => new Promise((resolve, reject) => {
                resolve(fs.unlinkSync(filename));
                console.log('Operation: Deleted old file');
              }).catch(() => console.log('Operation: failed to delete old file'))
                .then(() => {
                  fs.writeFile(filename, output, (error: string) => {
                    console.log('Operation: output new file');
                    console.log('hound dog fall asleep... 🐕');
                  })
                })
            );
          });
        });
    }
  );
};
