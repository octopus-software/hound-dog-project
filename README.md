# Hound-Dog

* TypeScript + Node.js project

# Usage


1.Run the following command

```bash
git clone git@gitlab.com:octopus-software/hound-dog-project.git
npm install
```

2.Rename `example.env` to `.env`  
3.Fill in the item in `.env` 

```
npm run dev:watch
```

# License

[ISC License](./LICENSE)
